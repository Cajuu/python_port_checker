from datetime import datetime
from re import match
from socket import AF_INET, SOCK_STREAM, error, socket
from sys import argv, exit
from threading import Thread, active_count
from time import sleep

DOMAINS_FILE = argv[1]
PORT         = argv[2]
THREADS      = argv[3]
TIMEOUT      = argv[4]
OUTPUT_FILE  = argv[5]


def check_arguments():
    if len(argv) != 6:
        exit("Wrong number of arguments\n")
    try:
        with open(DOMAINS_FILE):
            pass
    except IOError:
        exit("First argument should be a file containing domains!\n")
    try:
        int(PORT)
    except ValueError:
        exit("Second argument should be a number (port)!\n")

    try:
        int(THREADS)
    except ValueError:
        exit("Third argument should be a number (number of threads)!\n")

    try:
        int(TIMEOUT)
    except ValueError:
        exit("Fourth argument should be a number (timeout)!\n")

    try:
        with open(OUTPUT_FILE):
            pass
    except IOError:
        exit("Fifth argument should be a file!\n")


def get_domains():
    domains = []

    with open(DOMAINS_FILE) as domains_file:
        for line in domains_file:
            domains.append(line.rstrip())

    return domains


def check_domain_format(domain):
    clear_domain = match(r"(https?://(?:www\.)?|www\.)(.*)", domain)
    if clear_domain:
        return clear_domain.group(2)
    return domain


def check_for_open_ports():
    domains = get_domains()

    try:
        output_file = open(OUTPUT_FILE, 'a')
        for each_domain in domains:
            each_domain = check_domain_format(each_domain)

            t1 = Thread(target=check_for_open_port, args=(each_domain, output_file))
            t1.start()

            while active_count() > int(THREADS):
                sleep(int(TIMEOUT))
    except KeyboardInterrupt:
        exit("You pressed CTRL + C. Will exit now...\n")


def check_for_open_port(ip, output_file):
    try:
        sock = socket(AF_INET, SOCK_STREAM)
        result = sock.connect_ex((ip, int(PORT)))

        if result == 0:
            output_file.write(ip + '\n')
        sock.close()
    except error, e:
        print("[# ERROR #] Couldn't connect to server '{}' - {}!".format(ip, str(e)))
        # sock.close()


def main():
    print('[# DEV #]  Dexter'
          '{}\n'
          '[# INFO #] Usage:   python grj_checker.py [domains_file] [port_number] [nr_of_threads] [timeout] [output_file]\n'
          '[# INFO #] Example: grj_checker.py domains.txt 443 16 0 output.txt\n'
          '{}\n\n'.format('-' * 43, '-' * 110))

    check_arguments()

    t1 = datetime.now()
    check_for_open_ports()
    t2 = datetime.now()

    print('\n\n{}\n'
          '[# INFO #] Done processing domains!\n'
          '{}'.format('-' * 35, '-' * 35))
    print('Elapsed time: {} seconds'.format(t2 - t1))

if __name__ == '__main__':
    main()
